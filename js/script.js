let mainContainer = document.querySelectorAll(".quiz-main-container")[0]

let nextBtn = document.getElementById("next")
let previousBtn = document.getElementById("previous")

let firstAnsDiv = document.querySelectorAll(".answer-pool>div:nth-child(1)")[0]
let secondAnsDiv = document.querySelectorAll(".answer-pool>div:nth-child(2)")[0]
let thirdAnsDiv = document.querySelectorAll(".answer-pool>div:nth-child(3)")[0]

let radioButtons = document.querySelectorAll(".answer-pool input")
let answerPoolDivs = document.querySelectorAll(".answer-pool div")

let firstInput = document.getElementById("ans1")
let secondInput = document.getElementById("ans2")
let thirdInput = document.getElementById("ans3")

let firstAnsText = document.getElementById("ans1text")
let secondAnsText = document.getElementById("ans2text")
let thirdAnsText = document.getElementById("ans3text")

let currentQuestionNumberTag = document.querySelectorAll(".current-question-number")[0];
let totalQuestionNumberTag = document.querySelectorAll(".total-questions-number")[0];

let questionSelf = document.querySelectorAll(".question-self")[0]

let finishQuiz = document.querySelectorAll(".results-show")[0]


let currentQuestion = 1;
let usersAnswer = []


//============================= DATA
let questionsArr = [{
        question: `How do you call a function named "myFunction"?`,
        answers: ["call function myFunction()", "myFunction()", "call myFunction()"],
        correct: "myFunction()"
    },
    {
        question: "How to write an IF statement in JavaScript?",
        answers: ["if (i == 5)", " if i == 5 then", "if i = 5"],
        correct: "if (i == 5)"
    },
    {
        question: "How does a WHILE loop start?",
        answers: ["while i = 1 to 10", "while (i <= 10)", "while (i <= 10; i++)"],
        correct: "while (i <= 10)"
    },
    {
        question: "How do you write 'Hello World' in an alert box?",
        answers: [`msg("Hello World");`, `msgBox("Hello World");`, `alert("Hello World");`],
        correct: `alert("Hello World");`
    },
    {
        question: "How do you create a function in JavaScript?",
        answers: [`function = myFunction();`, `function myFunction();`, `function:myFunction()`],
        correct: `function myFunction();`
    }
]


let totalQuestionNumber = questionsArr.length;

console.log(questionsArr)




//======== Start Function
Init();



//================= NEXT QUESTION
nextBtn.addEventListener("click", function () {
    let currentIndex = currentQuestion - 1
    UserInputGetter(currentIndex);


    let index = parseInt(nextBtn.getAttribute('data-index'))
    QuestionPagination(index, 1) // 1 MEANS WE GO FORWARD

    MakeSelectedOnPagination(index)
});


//================= PREVIOUS QUESTION
previousBtn.addEventListener("click", function () {
    let currentIndex = currentQuestion - 1
    UserInputGetter(currentIndex);

    let index = parseInt(previousBtn.getAttribute('data-index'))
    QuestionPagination(index, 0) // 0 MEANS WE GO BACKWARD

    MakeSelectedOnPagination(index)
});





//================= INIT FUNCTION SETS EVERYTHING TO START
function Init() {
    QuesTionSetter(0);

    totalQuestionNumberTag.textContent = totalQuestionNumber
    currentQuestionNumberTag.textContent = currentQuestion

    previousBtn.style.display = "none"
    nextBtn.style.display = "block"
}



//=========================================== GO BETWEEN PAGES
function QuestionPagination(questionIndex, direction) {
    questionIndex = parseInt(questionIndex);

    //================= HANDLES VISIBILITY OF "NEXT, PREV" BUTTONS
    if (questionIndex === 0) {
        previousBtn.style.display = "none"
        nextBtn.style.display = "block"
    } else if (questionIndex === totalQuestionNumber - 1) {
        previousBtn.style.display = "block"
        nextBtn.style.display = "none"
    } else {
        previousBtn.style.display = "block"
        nextBtn.style.display = "block"
    }

    let prevIndex = parseInt(previousBtn.getAttribute('data-index'))
    let nextIndex = parseInt(nextBtn.getAttribute('data-index'))

    if (direction === 1) {
        prevIndex++;
        nextIndex++;
        currentQuestion++;
    } else if (direction === 0) {
        prevIndex--;
        nextIndex--;
        currentQuestion--;
    }

    nextBtn.setAttribute('data-index', nextIndex)
    previousBtn.setAttribute('data-index', prevIndex)

    totalQuestionNumberTag.textContent = totalQuestionNumber
    currentQuestionNumberTag.textContent = currentQuestion


    QuestionsHandlers(questionIndex);
}


//============================================== SHOWS RELEVANT QUESTION ACCODRING TO INDEX
function QuestionsHandlers(questionIndex) {
    QuesTionSetter(questionIndex);
    AnswerDefaultCase()
}


//================================================ MAKES INPUT FIELDS DEFAULT CONDITION
function AnswerDefaultCase() {
    for (let index = 0; index < radioButtons.length; index++) {
        radioButtons[index].checked = false;
        radioButtons[index].parentElement.style.backgroundColor = "lightgray"
    }
}


//============================================== GETS THE USER INPUT AND ADDS IN TO usersAnswer ARRAY
function UserInputGetter(questionIndex) {
    let selectedRadio = null;
    for (i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) {
            selectedRadio = radioButtons[i].value
        }
    }

    if (selectedRadio) {
        usersAnswer[questionIndex] = selectedRadio;
    } else {
        usersAnswer[questionIndex] = null;
    }

    console.log(usersAnswer)
}




//============================================================ EVEND HANDLER CHANGE STYLE OF SELECTED ANSWER
for (let index = 0; index < answerPoolDivs.length; index++) {
    answerPoolDivs[index].addEventListener("click", function () {

        answerPoolDivs.forEach(element => {
            element.style.backgroundColor = "lightgray"
        });

        radioButtons[index].checked = true;
        answerPoolDivs[index].style.backgroundColor = "gray"
    })
}



//============= MAKES USER SELECTED INPUT AGAIN SELECTED WHEN COMEBACK
function MakeSelectedOnPagination(questionIndex) {

    let userAnswer = usersAnswer[questionIndex];
    if (userAnswer == questionsArr[questionIndex].answers[0]) {
        radioButtons[0].parentElement.style.backgroundColor = "gray"
        radioButtons[0].checked = true;
    } else if (userAnswer == questionsArr[questionIndex].answers[1]) {
        radioButtons[1].parentElement.style.backgroundColor = "gray"
        radioButtons[1].checked = true;
    } else if (userAnswer == questionsArr[questionIndex].answers[2]) {
        radioButtons[2].parentElement.style.backgroundColor = "gray"
        radioButtons[2].checked = true;
    }
    

    QuesTionSetter(questionIndex);

}


//===================== FILLS THE QUESTION AND ANSWERS
function QuesTionSetter(questionIndex) {
    questionSelf.textContent = questionsArr[questionIndex].question;

    firstAnsText.textContent = questionsArr[questionIndex].answers[0];
    firstInput.value = questionsArr[questionIndex].answers[0];

    secondAnsText.textContent = questionsArr[questionIndex].answers[1];
    secondInput.value = questionsArr[questionIndex].answers[1];

    thirdAnsText.textContent = questionsArr[questionIndex].answers[2];
    thirdInput.value = questionsArr[questionIndex].answers[2];
}




//======================FINISHES THE QUIZ
finishQuiz.addEventListener("click", function () {
    if (currentQuestion == questionsArr.length) {
        let selectedRadio = null;
        for (i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].checked) {
                selectedRadio = radioButtons[i].value
            }
        }

        if (selectedRadio) {
            usersAnswer[(questionsArr.length-1)] = selectedRadio;
        } else {
            usersAnswer[(questionsArr.length-1)] = null;
        }
    }



    removeAllChildNodes(mainContainer)
    CreateTable()
})


//================ CLEARS THE CONTAINER TO REVIEW
function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}



//======================= CREATES REVIEW TABLE
function CreateTable() {

    for (let index = 0; index < questionsArr.length; index++) {

        let newDiv = document.createElement("div")
        newDiv.classList.add("answer-pool")



        let node = `
            <h1>Question: ${questionsArr[index].question}</h1>
            <div style="background-color: ${ ((usersAnswer[index]==questionsArr[index].answers[0]) && (questionsArr[index].answers[0]==questionsArr[index].correct)) ? 'green' : 'lightgray'}">
                <input type="radio" value=${questionsArr[index].answers[0]} disabled>
                <label>${questionsArr[index].answers[0]}</label>
            </div>

            <div style="background-color: ${ ((usersAnswer[index]==questionsArr[index].answers[1]) && (questionsArr[index].answers[1]==questionsArr[index].correct)) ? 'green' : 'lightgray'}">
                <input type="radio" value=${questionsArr[index].answers[1]} disabled>
                <label>${questionsArr[index].answers[1]}</label>
            </div>

            <div style="background-color: ${ ((usersAnswer[index]==questionsArr[index].answers[2]) && (questionsArr[index].answers[2]==questionsArr[index].correct)) ? 'green' : 'lightgray'}">
                <input type="radio" value=${questionsArr[index].answers[2]} disabled>
                <label>${questionsArr[index].answers[2]}</label>
            </div>
            <h3>Your Answer: ${usersAnswer[index] ? usersAnswer[index] : ""}</h3>
            <h3>Correct Answer: ${questionsArr[index].correct}</h3>
            <br/>
        `
        newDiv.innerHTML = node
        mainContainer.appendChild(newDiv)
    }

    
    let reloadButton = document.createElement("button")
    reloadButton.textContent = "Start Quiz Again"
    reloadButton.classList.add("reload-button")
    mainContainer.appendChild(reloadButton)

    let reloadButtonEvent = document.querySelectorAll(".reload-button")[0]
    reloadButtonEvent.addEventListener("click", function(){
        location.reload();
    });
}